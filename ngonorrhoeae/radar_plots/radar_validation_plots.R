library(ggradar)
library(ggplot2)
library(dplyr)
library(scales)
library(tibble)
library(reshape2)
library(RColorBrewer)
library(gridExtra)

abx <- c("AZM", "CRO", "CFM", "CIP", "TET", "PEN", "SPT")
add_missingABX <- function(abx, dataset){
  tmp_df <- data.frame(Agent=1, TOTAL=1, TP=1, FP=1, TN=1, FN=1, Precision=1, Sensitivity=1, Specificity=1, Negative.Predictive.Value=1, dataset=1)
  for (i in 1:length(abx)){
    tmp <- c(abx[i], rep(NaN, 9), dataset)   
    tmp_df <- rbind(tmp_df, tmp)
  }
  tmp_df <- tmp_df[-1,]
  tmp_df
}

ori_bench <- read.csv("485__test.csv")
ori_bench$dataset <- "test"
wh <- abx[which(!abx %in% ori_bench$Agent)]
ori_bench <- rbind(ori_bench, add_missingABX(wh, "test"))

val_all <- read.csv("485__validation.csv")
val_all$dataset <- "validation"

## Radar plots ##
ori_bench <- ori_bench[order(as.character(ori_bench$Agent)),]
val_all <- val_all[order(as.character(val_all$Agent)),]

##

sensitivity <- rbind(as.numeric(t(ori_bench[,"Sensitivity"])),
                     as.numeric(t(val_all[,"Sensitivity"])))
sensitivity <- as.data.frame(sensitivity)
rownames(sensitivity) <- c("inhouse", "validation")
colnames(sensitivity) <- ori_bench$Agent

sensitivity <- sensitivity[,-6]
sensitivity_radar <- sensitivity %>% 
  as_tibble(rownames = "group")

##

specificity <- rbind(as.numeric(t(ori_bench[,"Specificity"])),
                     as.numeric(t(val_all[,"Specificity"])))
specificity <- as.data.frame(specificity)
rownames(specificity) <- c("inhouse", "validation")
colnames(specificity) <- ori_bench$Agent

specificity <- specificity[,-c(6,7)]
specificity_radar <- specificity %>% 
  as_tibble(rownames = "group")

##

precision <- rbind(as.numeric(t(ori_bench[,"Precision"])),
                     as.numeric(t(val_all[,"Precision"])))
precision <- as.data.frame(precision)
rownames(precision) <- c("inhouse", "validation")
colnames(precision) <- ori_bench$Agent

precision <- precision[,-6]
precision_radar <- precision %>% 
  as_tibble(rownames = "group")

##

npv <- rbind(as.numeric(t(ori_bench[,"Negative.Predictive.Value"])),
                   as.numeric(t(val_all[,"Negative.Predictive.Value"])))
npv <- as.data.frame(npv)
rownames(npv) <- c("inhouse", "validation")
colnames(npv) <- ori_bench$Agent

npv <- npv[,-c(6,7)]
npv_radar <- npv %>% 
  as_tibble(rownames = "group")


sensit_radar <- ggradar(sensitivity_radar, plot.title="Sensitivity", base.size=12, group.point.size=3.5, grid.min=0, grid.mid=50, grid.max=100, plot.legend=FALSE)
specif_radar <- ggradar(specificity_radar, plot.title="Specificity",base.size=12, group.point.size=3.5, grid.min=0, grid.mid=50, grid.max=100, plot.legend=FALSE)
precision_radar <- ggradar(precision_radar, plot.title="Positive Predictive Value",base.size=12, group.point.size=3.5, grid.min=0, grid.mid=50, grid.max=100, plot.legend=FALSE)
npv_radar <- ggradar(npv_radar, plot.title="Negative Predictive Value",base.size=12, group.point.size=3.5, grid.min=0, grid.mid=50, grid.max=100, plot.legend=FALSE)

pdf("ggradar_plots.pdf", width=20, height=10)
grid.arrange(sensit_radar, specif_radar, precision_radar, npv_radar, ncol=4)
dev.off()

